using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI_EatFood : MonoBehaviour
{
    [SerializeField]
    private Transform food;
    

    private NavMeshAgent navMeshAgent;

   private void Awake()
   {
       navMeshAgent = GetComponent<NavMeshAgent>();
   }

   private void Update()
   {
       navMeshAgent.destination = food.position;

       
   }
}
